# Bosque

## Introducción

La oscuridad de la noche sumada a la cantidad de arboles del bosque hacían que correr fuera una tarea sumamente dificil, pero no teniamos opción si queríamos sobrevivir, teniamos que huir de ahí, estabamos cansados pero yo no deje de correr ni solté su mano en ningún momento, no iba a permitir que nada le pasara a la chica que amo, logramos llegar al claro donde se encontraba nuestra cabaña y corrimos hasta la puerta, entramos a la cabaña y cerré la puerta detras de nosotros mientras la bloqueaba con mi espalda, fue ahí cuando me permití verla de nuevo, ella estaba asustada, lloraba y no sabía que hacer, deje de bloquear la puerta y la abrace para tratar de calmarla.

Estabamos asustados pero al menos estabamos juntos, ella no quería soltarme y no la culpo, yo también estaba muerto del miedo.

Un golpe a la puerta nos estremeció, me puse delante de ella para protegerla, otro golpe, la puerta estaba a punto de romperse, si ese maldito quería llegar a ella tendría que ser por encima de mi cadaver, otro golpe mas fuerte que los anteriores, la puerta cedió al fin y el logró entrar, sentí un fuerte golpe, caí al piso y todo se nubló, alguien gritó pero todo se hacía cada vez más oscuro hasta que todo era completamente negro.


Desperté en el suelo de madera de la cabaña, estaba confundido pero no me dolía nada, no sentía frio ni calor ni miedo, solo sentía un vacio
